#!/usr/bin/python

# Python includes
import sys

# IMDB
import imdb

ia = imdb.IMDb()
            
if len(sys.argv) < 1 :
    print "You must provide a search term"
    exit();

searchTerm = sys.argv[1];

# Perform the search
results = ia.search_movie(searchTerm)

for item in results :
    print item.movieID, item['long imdb canonical title']

