/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "application.h"

#include <QtCore/QUrl>
#include <QtCore/QFile>
#include <QtCore/QRegExp>
#include <QtCore/QHash>

#include <QtGui/QInputDialog>

#include <KProcess>
#include <KCmdLineArgs>
#include <KDebug>
#include <KUrl>
#include <KTemporaryFile>
#include <KMessageBox>

#include <Soprano/PluginManager>
#include <Soprano/Parser>
#include <Soprano/Statement>
#include <Soprano/StatementIterator>

#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>
#include <nepomuk/datamanagement.h>
#include <nepomuk/storeresourcesjob.h>

#include <Nepomuk/Vocabulary/NIE>
#include <Nepomuk/Vocabulary/NFO>
#include <Soprano/Vocabulary/RDF>

using namespace Nepomuk::Vocabulary;
using namespace Soprano::Vocabulary;

Application::Application(bool GUIenabled): KApplication(GUIenabled)
{
    KCmdLineArgs *arguments = KCmdLineArgs::parsedArgs();
    if( arguments->count() < 1 ) {
        kWarning() << "URL not specified";
        exit(1);
        return;
    }

    setQuitOnLastWindowClosed(false);

    m_imdbOutput.open();
    m_searchOutput.open();

    KUrl url = arguments->url(0);
    m_filename = url.fileName();
    m_filename.replace('.', ' ');
    m_filename.replace('-', ' ');

    //TODO: Remove the extension
    //TODO: Remove special characters like [] ()
    //TODO: Remove DVDRip
    //TODO: Remove the year

    m_filename = "Step up";

    KProcess * searchProcess = new KProcess( this );
    // Need to make sure it finds it in the correct place
    QStringList args;
    args << "/home/vishesh/kde/src/nepomuk/imdb/search.py" << m_filename;
    searchProcess->setProgram( QLatin1String("python2"), args );
    searchProcess->setOutputChannelMode(KProcess::OnlyStdoutChannel);

    searchProcess->setStandardOutputFile( m_searchOutput.fileName() );

    connect(searchProcess, SIGNAL(finished(int)), this, SLOT(searchProcessFinished(int)));
    searchProcess->start();
}

void Application::searchProcessFinished(int returnCode)
{
    if( returnCode ) {
        kWarning() << "The search process returned an error code - " << returnCode;
        return;
    }

    QRegExp lineRegex("(\\d*)\\s*(.*)");
    QHash<QString, int> movieHash;
    QStringList items;

    QFile searchOutput(m_searchOutput.fileName());
    searchOutput.open(QIODevice::ReadOnly);
    QTextStream stream( &searchOutput );
    while( !stream.atEnd() ) {
        QString line = stream.readLine();
        if( line.indexOf(lineRegex) != -1 ) {
            int id = lineRegex.cap(1).toInt();
            QString movie = lineRegex.cap(2);

            movieHash.insert( movie, id );
            items << movie;
        }
    }

    QString choice = QInputDialog::getItem( 0, QLatin1String("Neopomuk IMDB Fetcher"),
                                            QLatin1String("Please choose a movie"), items );
    if( choice.isEmpty() ) {
        exit(0);
    }

    int movieId = movieHash[choice];

    KProcess * imdbProcess = new KProcess( this );
    QStringList args;
    args << "/home/vishesh/kde/src/nepomuk/imdb/imdbMovie.py" << QString::number(movieId);
    imdbProcess->setProgram( QLatin1String("python2"), args );
    imdbProcess->setOutputChannelMode(KProcess::OnlyStdoutChannel);
    imdbProcess->setStandardOutputFile( m_imdbOutput.fileName() );

    connect(imdbProcess, SIGNAL(finished(int)), this, SLOT(imdbProcessFinished(int)));
    imdbProcess->start();
}

namespace {

QHash<QUrl, Nepomuk::SimpleResource> toGraphHash(Soprano::StatementIterator& it) {
    QHash<QUrl, Nepomuk::SimpleResource> hash;

    while( it.next() ) {
        Soprano::Statement st = it.current();
        const QString subUri = st.subject().toN3();

        Nepomuk::SimpleResource res;
        QHash<QUrl, Nepomuk::SimpleResource>::iterator fit = hash.find( subUri );
        if( fit != hash.end() )
            res = fit.value();
        
        res.setUri( subUri );

        QVariant value;
        Soprano::Node obj = st.object();
        if( obj.isBlank() ) {
            value = obj.toN3();
        } 
        else if( obj.isLiteral() ) {
            value = obj.literal().variant();
        }
        else if( obj.isResource() ) {
            value = obj.uri();
        }

        res.addProperty( st.predicate().uri(), value );
        hash[subUri] = res;
    }

    return hash;
}

}

void Application::imdbProcessFinished(int returnCode)
{
    if( returnCode ) {
        kWarning() << "The IMDB process returned an error code - " << returnCode;
        return;
    }

    Soprano::PluginManager *manager = Soprano::PluginManager::instance();
    const Soprano::Parser *parser = manager->discoverParserForSerialization(Soprano::SerializationNQuads);
    kWarning() << m_imdbOutput.fileName();
    Soprano::StatementIterator it = parser->parseFile( m_imdbOutput.fileName(),
                                                       QUrl(), Soprano::SerializationNQuads );

    // Convert the statement list to a SimpleResourceGraph
    QHash<QUrl, Nepomuk::SimpleResource> hash = toGraphHash( it );
    if( hash.isEmpty() ) {
        kWarning() << "Could not be converted into a Nepomuk::SimpleResourceGraph";
        return;
    }
    hash[ QUrl("_:mainResource%20") ].addProperty( NIE::url(), m_filename );
    hash[ QUrl("_:mainResource%20") ].addProperty( RDF::type(), NFO::FileDataObject() );

    Nepomuk::SimpleResourceGraph graph;
    QHash<QUrl, Nepomuk::SimpleResource>::const_iterator iter = hash.constBegin();
    for(; iter!=hash.constEnd(); iter++) {
        graph << iter.value();
    }

    KJob *job = Nepomuk::storeResources( graph );
    job->exec();

    if( job->error() ) {
        KMessageBox::error( 0, job->errorString() );
        exit(1);
    }
    else {
        KMessageBox::information( 0, QLatin1String("Pushed data into Nepomuk") );
        exit(0);
    }
}

