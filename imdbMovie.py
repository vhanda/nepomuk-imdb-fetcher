#!/usr/bin/python

# Python includes
import sys
import random

# Qt Includes
import PyQt4.QtCore as QtCore
import PyQt4.QtGui as QtGui

from PyQt4.QtCore import QDate
from PyQt4.QtCore import QString
from PyQt4.QtCore import QTextStream

# KDE Includes
from PyKDE4.kdecore import ki18n, KAboutData, KCmdLineArgs, KCmdLineOptions
from PyKDE4.kdecore import KUrl
from PyKDE4.kdeui import KApplication
from PyKDE4.kdeui import KListWidget

import PyKDE4.kdecore as kdecore
import PyKDE4.kdeui as kdeui

from PyKDE4.kio import KIO

# Nepomuk Includes
from PyKDE4.nepomuk import Nepomuk
from PyKDE4.soprano import Soprano

# Vocabulary
RDF = Soprano.Vocabulary.RDF
NAO = Soprano.Vocabulary.NAO
NIE = Nepomuk.Vocabulary.NIE
NMM = Nepomuk.Vocabulary.NMM
NCO = Nepomuk.Vocabulary.NCO
NFO = Nepomuk.Vocabulary.NFO

# IMDB
import imdb

ia = imdb.IMDb()

class RdfConverter :
    def __init__(self, result) :
        self.result = result
        self.resource = Soprano.Node.createBlankNode("mainResource")
        self.stList = []
        self.blankUri = 1

        self.stList.append( Soprano.Statement( self.resource, Soprano.Node(RDF.type()),
                                               Soprano.Node(NFO.Video()) ) )
        for key in result.keys() :
            self.handle( key, result[key] )

    def handle_people( self, people ) :
        if type(people) == list :
            uriList = []
            for person in people :
                uriList.append( self.handle_person( person ) )
            return uriList
        else :
            return self.handle_person( people )

    def getBlankUri( self ) :
        uri = str(self.blankUri);
        self.blankUri += 1 

        return uri

    def handle_person( self, person ) :
        if type(person) != imdb.Person.Person :
            return

        # FIXME: Implement a more fool proof way
        uri = Soprano.Node.createBlankNode( self.getBlankUri() )

        self.stList.append( Soprano.Statement( uri, Soprano.Node(RDF.type()),
                                               Soprano.Node(NCO.Contact()) ) )
        object = Soprano.Node(Soprano.LiteralValue(str(person)))
        self.stList.append( Soprano.Statement( uri,
                                               Soprano.Node(NCO.fullname()), \
                                               object ) )
        return uri


    def add_person_list( self, prop, value ) :
        people = self.handle_people( value )
        for person in people :
            st = Soprano.Statement( self.resource, \
                                    Soprano.Node(prop), \
                                    person )
            self.stList.append( st )

    def handle( self, key, value ) :
        if key == "ratings" :
            st = Soprano.Statement( self.resource, \
                                    Soprano.Node(NAO.numericRating()), \
                                    Soprano.Node(Soprano.LiteralValue( value )) )
            self.stList.append( st )

        elif key == "year" :
            date = QDate( int(value), 1, 1 )
            lv = Soprano.LiteralValue( date )
            st = Soprano.Statement( self.resource, \
                                    Soprano.Node(NMM.releaseDate()), \
                                    Soprano.Node(Soprano.LiteralValue( date )) )
            self.stList.append( st )

        elif key == "title" :
            st = Soprano.Statement( self.resource, \
                                    Soprano.Node(NIE.title()), \
                                    Soprano.Node(Soprano.LiteralValue( value )) )
            self.stList.append( st )

        elif key == "mpaa" :
            st = Soprano.Statement( self.resource, \
                                    Soprano.Node(NMM.audienceRating()), \
                                    Soprano.Node(Soprano.LiteralValue( value )) )
            self.stList.append( st )

        elif key == "writer" :
            self.add_person_list( NMM.writer(), value )

        elif key == "cinematographer" :
            self.add_person_list( NMM.cinematographer(), value )

        elif key == "genres" :
            for genre in value :
                st = Soprano.Statement( self.resource, \
                                        Soprano.Node(NMM.genre()), \
                                        Soprano.Node(Soprano.LiteralValue( genre )) )
                self.stList.append( st )

        elif key == "director" :
            self.add_person_list( NMM.director(), value )

        elif key == "plot" :
            st = Soprano.Statement( self.resource, \
                                    Soprano.Node(NMM.synopsis()), \
                                    Soprano.Node(Soprano.LiteralValue( str(value) )) )
            self.stList.append( st )

        elif key == "cast" :
            self.add_person_list( NMM.actor(), value )

        #elif key == "full-size cover url" :
            #FIXME: Handle me

if __name__ == "__main__" :
    if len(sys.argv) < 1 :
        print "Must provide movie id"
        exit()

    movieId = sys.argv[1];
    movie = ia.get_movie(movieId)
    rdf = RdfConverter( movie )

    plugin_manager = Soprano.PluginManager.instance()
    serializer = plugin_manager.discoverSerializerForSerialization( Soprano.SerializationNQuads );

    it = Soprano.Util.SimpleStatementIterator( rdf.stList )
    string = QString()
    stream = QTextStream(string)
    serializer.serialize( it, stream, Soprano.SerializationNQuads )

    # For removing the trailing \n
    print unicode(string.toUtf8()[:-1], "utf-8")
    #for st in rdf.stList :
    #    print st.subject().toN3(), st.predicate().toN3(), st.object().toN3()

